import pyxel
# import random

# =========================================================
# == Initialisation
# =========================================================

pyxel.init(210, 100, title="intr")

#terr = zone jouable
# 0 = rien  |  1 = sol  |  2 = mur  | 3 = Obstacle 1  |  4 = balle |  5 = Joueur torse  |  6 = Joueur jambes | 7 = Monstre torse | 8 = Monstre jambes


terr = [#0  1  2  3  4  5  6  7  8  9  10 11 12 13 14 15 16 17 18 19 20
        [2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2], # 0
        [2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2], # 1
        [2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2], # 2
        [2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2], # 3
        [2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2], # 4
        [2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2], # 5
        [2, 0, 0, 0, 0, 0, 0, 0, 0, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2], # 6
        [2, 0, 0, 0, 0, 0, 0, 0, 3, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2], # 7
        [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1], # 8
        [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]  # 9
       ]

torse_p = [2,6]
jambes_p = [2,7]
balles_p = []
vmonstre = 5
hit = 0

torse_m = [14,6]
jambes_m = [14,7]


# =========================================================
# == Fonctions d'arrière plan
# =========================================================


def avancer ():
    """Fait avancer le personnage d'une case vers la droite
    """
    global torse_p, jambes_p
    if terr[jambes_p[1]][jambes_p[0] + 1] == 0 and torse_m[0]-1 != torse_p[0] :
        #perso vers position suivante
        torse_p[0] = torse_p[0] + 1
        jambes_p[0] = jambes_p[0] + 1

    if terr[jambes_p[1]+1][jambes_p[0]] == 0 : #physique de perso
        torse_p[1] = jambes_p[1]
        jambes_p[1] = jambes_p[1] + 1

    return torse_p, jambes_p

# =========================================================

def reculer ():
    """Fait reculer le personnage d'une case vers la gauche
    """
    global torse_p, jambes_p
    if terr[jambes_p[1]][jambes_p[0] - 1] == 0 :
        #perso vers position precedente
        torse_p[0] = torse_p[0] - 1
        jambes_p[0] = jambes_p[0] - 1

    if terr[jambes_p[1]+1][jambes_p[0]] == 0 : #physique de perso
        torse_p[1] = jambes_p[1]
        jambes_p[1] = jambes_p[1] + 1
    return torse_p, jambes_p

# =========================================================

def escalader ():
    #regarder si devant obstacle
    if terr[jambes_p[1]][jambes_p[0]+1] == 3 :

        #perso vers Y suivant
        torse_p[1] = torse_p[1] - 1
        jambes_p[1] = jambes_p[1] - 1
        #perso vers X suivant
        torse_p[0] = torse_p[0] + 1
        jambes_p[0] = jambes_p[0] + 1
    print("j",jambes_p,"t",torse_p)
    return torse_p, jambes_p

# =========================================================

def ajoute_balle():
    """Ajoute une balle sur la carte
    """

    global balles_p

    balle_p = torse_p[0]+1, torse_p[1]
    if terr[balle_p[1]][balle_p[0]] == 0 :
        terr[balle_p[1]][balle_p[0]] = 4
        balles_p.append(balle_p)
    return balles_p

# =========================================================

def dessin_balle():
    """Déplace les balles vers l'avant sur la carte jusqu'a collision
    """
    global balles_p,vmonstre

    new_balles_p = []
    for i in range(len(balles_p)) :
        balle_p = balles_p.pop(0)

        if terr[balle_p[1]][balle_p[0]+1] == 2 or terr[balle_p[1]][balle_p[0]+1] == 3 :
            terr[balle_p[1]][balle_p[0]] = 0


        if balle_p[0]+1 == torse_m[0] :
            vmonstre = vmonstre - 1
            terr[balle_p[1]][balle_p[0]] = 0

        elif terr[balle_p[1]][balle_p[0]+1] == 0 :
            terr[balle_p[1]][balle_p[0]] = 0
            balle_p = balle_p[0]+1,balle_p[1]
            new_balles_p.append(balle_p)
            terr[balle_p[1]][balle_p[0]] = 4

    balles_p = new_balles_p.copy()

# =========================================================

def deplacement_monstre():
    """Déplace les monstres en direction du joueur
    """
    global torse_m, jambes_m

    if torse_m[0]-1 != torse_p[0] :
        print(1)
        if terr[torse_m[1]][torse_m[0]-4] == 0:
            if torse_p[0]-8 <= torse_m[0] and torse_p[1] == torse_m[1] :
                print(3)

                torse_m[0] = torse_m[0] - 1
                jambes_m[0] = jambes_m[0] - 1
    return torse_m, jambes_m

# =========================================================

def apparition_monstre():
    """Applique l'un nouveau monstre sur la carte
    """
    terr[torse_m[0]][torse_m[1]] = 7
    terr[jambes_m[0]][jambes[1]] = 8




# =========================================================
# == UPDATE
# =========================================================
def update():
    """mise à jour des variables (30 fois par seconde)"""

    global torse_p, jambes_p, balles_p

    deplacement_monstre()

    if pyxel.btn(pyxel.KEY_RIGHT):
        torse_p, jambes_p = avancer()

    if pyxel.btn(pyxel.KEY_LEFT):
        torse_p, jambes_p = reculer()

    if pyxel.btnp(pyxel.KEY_UP):
        torse_p, jambes_p = escalader()

    if pyxel.btnp(pyxel.KEY_SPACE):
        balles_p = ajoute_balle()

    if balles_p != [] :
        dessin_balle()



# =========================================================
# == DRAW
# =========================================================
def draw():
    """création des objets (30 fois par seconde)"""
    global ii, balles_p, torse_p, jambes_p, torse_m, jambes_m
    # vide la fenetre
    pyxel.cls(0)


    # Affichage du terrain
    for i in range (len(terr)) :
        for ii in range(len(terr[i])):
            if terr[i][ii] == 2 :
                pyxel.rect(ii*10, i*10,10,10,13)
            elif terr[i][ii] == 1 :
                pyxel.rect(ii*10, i*10,10,10,7)
            elif terr[i][ii] == 3 :
                pyxel.rect(ii*10, i*10,10,10,15)
            elif terr[i][ii] == 7 :
                pyxel.rect(ii*10, i*10,10,10,8)
            elif terr[i][ii] == 8 :
                pyxel.rect(ii*10, i*10,10,10,2)
            elif terr[i][ii] == 4 :
                pyxel.rect(ii*10, i*10,10,10,10)


    # Affichage du personage (carre 8x8)
    pyxel.rect(torse_p[0]*10, torse_p[1]*10,10,10,1)
    pyxel.rect(jambes_p[0]*10, jambes_p[1]*10,10,10,5)

    pyxel.rect(torse_m[0]*10, torse_m[1]*10,10,10,8)
    pyxel.rect(jambes_m[0]*10, jambes_m[1]*10,10,10,2)


# =========================================================
# == RUN
# =========================================================

pyxel.run(update, draw)